<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('home');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/register', function () {
    return view('register');
});
Route::get('/welcome', function () {
    return view('welcome');
});
Route::get('/userif', function () {
    return view('userif');
});
Route::get('/item', function () {
    return view('item');
});
Route::get('/editpro', function () {
    return view('editpro');
});
Route::get('/edit', function () {
    return view('edit');
});
Route::get('/new', function () {
    return view('new');
});
Route::resource('admin','AdminController');
Route::resource('User','UserController');
Route::post('/check','UserController@checklogin');
Route::post('/adduser','UserController@adduser');
//Route::post('/post','UserController@index');
//Route::post('/updata','UserController@updata');
