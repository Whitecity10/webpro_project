@extends('master')
@section('title','Login/Register')
@section('content')
<div class="container">
 <div class="row">
 <div class="col-lg-10 col-log-offset-1">
 <div class="card mt-3">
 <div class="card-header h3">
 แก้ไขข้อมูล
 <?= Form::model($user, array('url' => 'admin/' . $user->id,
 'method' => 'put','files' => true)) ?>
<div class="form-group">
 <?= Form::label('username', 'Username');
?>
 <?= Form::text('username',null, ['class' =>
'form-control', 'placeholder' => 'Username']);
?>
 </div>
<div class="form-group">
 {!! Form::label('email', 'อีเมล'); !!}
{!! Form::text('email',null,['class' =>'form-control','placeholder' => 'name@gmail.com']); !!}
 </div>
<div class="form-group">
 <?= Form::label('password', 'ชื่อ-นามสกุล');
?>
 <?= Form::text('password' , isset($users->owners->fullname)? $users->owners->fullname:null, ['class' =>
'form-control', 'placeholder' => 'ชื่อ-นามสกุล']);
?>
 </div>

  <div class="form-group">
 <?= Form::label('email', 'ที่อยู่');
?>
 <?= Form::text('email',  isset($users->owners->address)? $users->owners->address:null, ['class' =>
'form-control', 'placeholder' => 'ที่อยู่']);
?>
 </div>
 <div class="form-group">
 <?= Form::label('card', 'โทรศัทพ์');
?>
 <?= Form::text('card',isset($users->owners->tel)? $users->owners->tel:null, ['class' =>
'form-control', 'placeholder' => 'โทรศัทพ์']);
?>
 </div>
<div class="form-group">
 <?= Form::label('status', 'รหัสพนักงาน');
?>
 <?= Form::text('status', isset($users->owners->staff_id)? $users->owners->staff_id:null, ['class' =>
'form-control', 'placeholder' => 'รหัสพนักงาน']);
?>
 </div>

 <div class="form-group">
 <?= Form::submit('แก้ไขข้อมูล', ['class' => 'btn btn-primary']); ?>
 </div>
 {!! Form::close() !!}
 </div>
 </div>
 </div>
 </div>
</div>
@endsection
