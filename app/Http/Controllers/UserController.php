<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /*  $user = User::all();
        return true;*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('user.register');
    }
    public function checklogin(Request $request){
      $data = $request->all();
      $user = $data['username'];
      $pass = $data['password'];
      $array = array(
        'username' => $user,
        'password' => $pass
      );
      $ans = DB::table('users')->where($array)->get();
      $row_count = count($ans);
      if($row_count == 1){
        return redirect('userif');
      }
        else {
          $message = "Fail";
          //return redirect('login');
        //  <script type="text/javascript">alert("hello!");</script>
          //return redirect('login')->with('jsAlert', $message);
        }


    }

    public function adduser(Request $request){
      //$user->validate($request,['username' => 'require','password' => 'require']);
      $user = new User();

      $user->username = $request->username;
      $user->password = $request->password;
      $user->email = $request->email;
      $user->card = $request->card;
      $user->status = "user";

      if($user->save()){
        return redirect('login');

      }else {
        return redirect('register');
      }
    }
    public function updata(Request $request,User $usermodel){
      if($usermodel->fill($request->all())->save()){
        return $request;
      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request,['username' => 'require','password' => 'require']);
        // $user = new User(
        //   [
        //     'username' => $request->get('username'),
        //     'password' => $request -> get('password')
        //   ]
        //   );
        // $user-save();
        // return redirect()->route('user.index')->with('success','Saved');
      }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
